<?php

return [

    'name'=>'Name',
    'id'=>'Id',
    'search'=>'Search',
    'showing'=>'Showing',
    'to'=>'to',
    'of'=>'of',
    'entries'=>'entries',
    'all'=>'All',

    'dashboard'=>'Dashboard',

    'CopyRights'=>'2016 &copy; Elite Admin brought to you by themedesigner.in',

    'language'=>'Language',


    'notification'=>'Notification',
    'setting'=>'Setting',
    'basic'=>'Basic',
    'edit_config'=>'Edit Config',
    'roles'=>'Roles',
    'rolesCreate'=>'Create Role',
    'created_at'=>'Created At',
    'updated_at'=>'Updated At',
    'logout'=>'Logout',

    'authentication'=>'Authentication',
    'authenticationList'=>'Authentication List',
    'authenticationCreate'=>'Create Authentication ',
    'authenticationMenuBottomHeader'=>'AuthenticationMenu Bottom Header',
    'authenticationMenuBottomDescription'=>'Authentication Menu Bottom Description',



    'layout'=>'Layout',
    'layoutList'=>'Layout List',
    'layoutCreate'=>'Create Layout ',
    'layoutMenuBottomHeader'=>'LayoutMenu Bottom Header',
    'layoutMenuBottomDescription'=>'Layout Menu Bottom Description',




    'role'=>'Role',
    'roleList'=>'Role List',
    'roleCreate'=>'Create Role ',
    'roleMenuBottomHeader'=>'RoleMenu Bottom Header',
    'roleMenuBottomDescription'=>'Role Menu Bottom Description',


    'user'=>'User',
    'userList'=>'User List',
    'userCreate'=>'Create User ',
    'userMenuBottomHeader'=>'UserMenu Bottom Header',
    'userMenuBottomDescription'=>'User Menu Bottom Description',

];